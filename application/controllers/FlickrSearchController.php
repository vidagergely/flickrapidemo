<?php

class FlickrSearchController extends My_Base
{

	private $_flickrAPIKey = '';

	public function init()
  {
			parent::init();
			$flickr = $this->_bootstrap->getOption('flickr');
			$this->_flickrAPIKey = $flickr['APIKey'];
			if(!$this->_flickrAPIKey) {
				throw new Exception('Flickr APIKey not filled.');
			}
	}

	public function getAction()
  {
		$q = $this->_getParam('q', false);

		// default
		$this->getResponse()->setHttpResponseCode(My_Base::BAD_REQUEST);
		$this->view->success = 'false';

		// validate parameter
		if($q) {
			// TODO: chcek flickr error codes
			$result = $this->_search($q);
			$this->getResponse()->setHttpResponseCode(My_Base::OK);
			$this->view->result = $result;
			$this->view->success = 'true';
		} else {
			$this->getResponse()->setHttpResponseCode(My_Base::BAD_REQUEST);
			$this->view->success = 'false';
			$this->view->error = 'missing parameter';
		}
	}

	// search on flickr by tag
	private function _search($q) {

//		echo "<a href='http://www.flickr.com/photos/" . $photo['owner'] . "/" . $photo['id'] . "/'>";
//		echo $photo['title'];

		$phpFlickr = new phpFlickr($this->_flickrAPIKey);
		$result = $phpFlickr->photos_search(array('tags' => $q, 'per_page' => 10));

		// TODO: make it better, too slow
		foreach($result['photo'] as $key => $item) {
			$sizes = $phpFlickr->photos_getSizes($item['id']);
			$result['photo'][$key]['thumbnail'] = $sizes[1];

			//$info = $phpFlickr->photos_getInfo($item['id']);
			//$result['photo'][$key]['info'] = $info;
		}

		return $result;
	}


}
