<?php

class KeywordController extends My_Base
{

	const FILE = '/configs/tags.json';

	protected $_tree = false;
	protected $_result = array();

	public function init()
  {
			parent::init();
			$this->_tree = file_get_contents($this->_getFilePath());
			$this->_tree = json_decode($this->_tree, true);
	}

	// list tags
	public function indexAction() {
		$this->_result[] = array(
			'id' => 'root',
			'level' => 0,
			'label' => $this->_tree['root']
		);
		$this->getItem($this->_tree['root']);

		$this->getResponse()->setHttpResponseCode(My_Base::OK);
		$this->view->result = $this->_result;
		$this->view->success = 'true';
	}

	public function getItem($item, $level = 0) {
		if(isset($this->_tree[$item]) && is_array($this->_tree[$item])) {
			$level++;
			foreach ($this->_tree[$item] as $key => $value) {
				$this->_result[] = array(
					'id' => $value,
					'level' => $level,
					'label' => html_entity_decode(str_repeat('&nbsp;', $level * 4) . $value)
				);
				$this->getItem($value, $level);
			}
			$level--;
		}

	}

	private function _deleteItem($item, $id) {
		if(isset($this->_tree[$id])) {
			unset($this->_tree[$id]);
		}
		if(isset($this->_tree[$item]) && is_array($this->_tree[$item])) {
			foreach ($this->_tree[$item] as $key => $value) {
				if($value == $id) {
					unset($this->_tree[$item][$key]);
				}
				$this->_deleteItem($value, $id);
			}
		}

	}

	private function _addItem($item, $id, $new) {
		if(isset($this->_tree[$item]) && is_array($this->_tree[$item])) {
			foreach ($this->_tree[$item] as $key => $value) {
				if($value == $id) {
					if(is_array($this->_tree[$item][$key])) {
						$this->_tree[$item][$key][] = $new;
					} else {
						//$this->_tree[$item][$key] = $new;
					};
				}
				$this->_addItem($value, $id, $new);
			}
		}

	}

	private function _writeToFile($config) {
		$fp = fopen($this->_getFilePath(), 'w');
		fwrite($fp, json_encode($config));
		fclose($fp);
	}

	private function _getFilePath() {
		return APPLICATION_PATH . self::FILE;
	}

	public function putAction() {

		$id = $this->_getParam('id');
		$value = $this->_getParam('value');

		$this->_addItem($this->_tree['root'], $value);
		$this->_tree[$id][] = $value;

		$this->_writeToFile($this->_tree);

		$this->getResponse()->setHttpResponseCode(201);
		$this->view->result = 'Created';
		$this->view->success = 'true';

		//print '<pre>';
		//var_dump($this->_tree);
	}

	public function deleteAction() {

		$id = $this->_getParam('id');

		$this->_deleteItem($this->_tree['root'], $id);
		$this->_writeToFile($this->_tree);

		$this->getResponse()->setHttpResponseCode(201);
		$this->view->result = 'Deleted';
		$this->view->success = 'true';

		//print '<pre>';
		//var_dump($this->_tree);
	}

}
