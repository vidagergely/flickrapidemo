
#!/bin/bash
ftp_site=ursus.dima.hu
username=vidagergely
passwd=`echo base64password | base64 --decode`
remote=/www/x
folder=$1
cd /Users/gerq/http/flickrapiproject/vagrant/flickrapi/$folder
pwd
ftp -in <<EOF
open $ftp_site
user $username $passwd
mkdir $remote/$folder
cd $remote/$folder
mput *
close
bye
