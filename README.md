# Flickr API demo #

Search images through Flickr API with Zend 1 and Angular JS.

### How to use ###

* curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
* cd [lib dir]
* composer require zendframework/zendframework1
* composer require wikia/phpflickr
* http://flickrapi.dev
* chmod +x flickrapidemo/bin/deployftp.sh
* set host, dirs, password in deployftp.sh
* deploy to ftp server: find . -type d -exec ./bin/deployftp.sh {} \;
